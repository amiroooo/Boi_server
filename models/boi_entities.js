'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
require('mongoose-double')(mongoose);	
var SchemaTypes = mongoose.Schema.Types;
	
var JobCategoriesSchema = new Schema({
    jobCategoryName: {
        type: String,
        required: true,
        unique: true
    },
    jobDescription: {
        type: String
    },
	imageUrl:{
		type: String,
		unique: true
	}
});

var JobSubCategoriesSchema = new Schema({
	categoryId: {
        type: Schema.Types.ObjectId,
		ref: 'JobCategoriesSchema',
		required: true
    },
    subCategoryName: {
        type: String,
        required: true
    },
    subCategoryDescription: {
        type: String
    }
});




var JobsSchema = new Schema({
	ownerId: {
        type: Schema.Types.ObjectId,
		ref: 'UserSchema',
        required: true
    },
    categoryId: {
        type: Schema.Types.ObjectId,
		ref: 'JobCategoriesSchema',
        required: true
    },
	subCategoryId: [{ 
					  type: Schema.Types.ObjectId, 
					  ref: 'JobSubCategoriesSchema' 
				   }] ,
    jobName: {
        type: String,
		required: true
    },
    jobDescription: {
        type: String
    },
    jobLocationName: {
        type: String,
		required : true
    },
	imageUrl:{
		type: String,
		unique: true
	},
	geo: {type: [SchemaTypes.Double],
		index: '2d',
		required : true
	}
});

var UserSchema = new Schema({
	login:{
        type: String,
        required: true,
		unique: true		
	},
	_password:{
        type: String,
        required: true		
	},
	salt:{ // hash code to use in order to compare the user typed password with the stored one
        type: String,
        required: true		
	},
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
		required: true
    },
    facebook_id: {
        type: String
    },
    google_plus_id: {
        type: String
    },
	token: {
        type: String	
	},
	role: {
		type: String,
		default: 'bearer'
	}
});


var FavouritesSchema = new Schema({
	userId:{
		type: Schema.Types.ObjectId, 
		ref: 'UserSchema', 
		required: true
	},
	jobId:{
		type: Schema.Types.ObjectId, 
		ref: 'JobsSchema', 
		required: true
	}	
	
});


var NotificationsSchema = new Schema({
	jobId:{
		type: Schema.Types.ObjectId, 
		ref: 'JobsSchema', 
		required: true
	},
	subject:{
		type: String,
		required: true
	},	
	description:{
		type: String,
		required: true
	},
	type:{ // grip soft hard
		type: String,
		default: 'soft'
	},	
	accessibility:{
		type: String,
		required: true,
		default: 'public'
	}
	
});


mongoose.model('JobCategories', JobCategoriesSchema);
mongoose.model('JobSubCategories', JobSubCategoriesSchema);	
mongoose.model('Jobs', JobsSchema);	
mongoose.model('User', UserSchema);
mongoose.model('Favourite', FavouritesSchema);
mongoose.model('Notifications', NotificationsSchema);


	

	
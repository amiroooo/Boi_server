var express = require('express'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser');



config = require('./config/config.js');
app = express();

app.use(bodyParser.json({defer: true}));
                app.use(bodyParser.urlencoded({
                    extended: true,
                    defer: true
                }));



app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));

require('./routes/app_routes').appRoutes();
require('./routes/add_to_db_routes').addToDbRoutes();

mongoose.Promise = global.Promise;
mongoose.connect(config.appconf.db, config.appconf.db_options, function (err) {
	if(err){
		console.log('Error connecting to db',err);
	}else{
        console.log('connected to db');
  }
});

config.appconf.bind_port = process.env.PORT || config.appconf.bind_port;
console.log("BOI API server @ "+config.appconf.bind_port+" ");
app.listen(config.appconf.bind_port);

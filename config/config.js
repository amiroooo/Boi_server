module.exports.appconf = {
  bind_port:'443',
  db: 'mongodb://ds117136.mlab.com:17136/boi_db',

  db_options:{
    auto_reconnect: 'true',
    user: 'server',
    pass: '88888888',
	useMongoClient: true
  },
  request_timeout: 10000,
  server_url: 'http://default-environment.9cpuixfwcp.us-west-2.elasticbeanstalk.com/',
  s3_options:{ 
	"bucket": "elasticbeanstalk-us-west-2-206359030210", 
	"region": "us-west-2" 
  },
  jwt_secret: "boi_server_secret",
  jwt_token_expiration_time : 10
};
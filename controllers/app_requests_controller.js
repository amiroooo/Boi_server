var async = require('async'),
    BOIModel = require('../models/boi_entities.js');
	mongoose = require('mongoose'),
    JobCategories = mongoose.model('JobCategories'),
	JobSubCategories = mongoose.model('JobSubCategories'),
	Jobs = mongoose.model('Jobs'),
	User = mongoose.model('User'),
	Favourite = mongoose.model('Favourite'),
	Notifications = mongoose.model('Notifications'),
    config = require('../config/config.js'),
    MongoClient = require('mongodb').MongoClient;
var jwt = require('jsonwebtoken');	
const crypto = require('crypto');
	
	
exports.getJobCategories = function (req, res) {
	console.log('Getting Job Categories');

	JobCategories.find({}).exec(function (err, categories) {
                if (err) {
                    return res.send("Error retrieving Job Categories: " + err);
                } else {
                    return res.send(categories);
                }
            });

}

exports.getSubCategories = function (req, res) {
	console.log('Getting Sub Categories');
	var category_id = req.body[0];
	JobSubCategories.find({categoryId: category_id}).exec(function (err, subcategories) {
                if (err) {
                    return res.send("Error retrieving Job JobSubCategories: " + err);
                } else {
                    return res.send(subcategories);
                }
            });

}

exports.getNotifications = function (req, res) {
	console.log('Getting Notifications');
	var querystr = req.params.querystr;
	Notifications.find({jobId: querystr}).populate({ path: 'jobId', model: Jobs }).exec(function (err, notifications) {
                if (err) {
                    return res.send("Error retrieving Job Notifications: " + err);
                } else {
					
				res.render('../views/pages/notifications', {
					notifications: notifications,
					jobName: notifications[0].jobId.jobName
				});
                }
            });

}



exports.getJobDetails = function (req, res) {
	console.log('Getting Job Details');
	var querystr = req.params.querystr;
	console.log('Getting Job Details, id: ' +querystr);
	Jobs.findOne({_id: querystr}).exec(function (err, job) {
                if (err) {
                    return res.send("Error retrieving Job: " + err);
                } 
				if (job == null){
					res.send({});
				}else
					res.send(job);
            });

}



exports.getAppLink = function (req, res) {
	console.log('Applink confirmation');
	
	return res.send([{
  "relation": ["delegate_permission/common.handle_all_urls"],
  "target": {
    "namespace": "android_app",
    "package_name": "com.example.amir.businessofinterest",
    "sha256_cert_fingerprints":
    ["F8:1A:37:8E:41:F1:E7:85:7B:51:22:8A:97:37:7E:5C:C0:78:10:B7:34:39:D4:C9:3D:C4:30:06:63:5F:C1:20"]
  }
}]);
} 

exports.test = function (req, res) {
	console.log('test');
	
	res.set('Content-Type', 'text/html');
	res.send(new Buffer('<html><title></title><head></head><body><a href="http://businessofinterest.com/jobid/23523523">Test String</a></body><html>'));
} 

exports.getFavorites = function (req, res) {
	console.log('Getting Favourites');
	var user_id = req.body[0];	
	Favourite.find({userId: user_id}).populate({ path: 'jobId', model: Jobs }).exec(function (err, jobs) {
                if (err) {
					console.log("err: " + err);
                    return res.send("Error retrieving Jobs in Favorites: " + err);
                } else {
                    return res.send(jobs);
                }
            });	
}




exports.getJobsByDistance = function (req, res) {
	console.log('Getting Job by distance');
	var lat = req.body[0];
	var lng = req.body[1];
	var distance_raw = req.body[2]; // in meters
	
	
	var distance = distance_raw / 6371;

	var query = City.findOne({'geo': {
	  $near: [
		lat,
		lng
	  ],
	  $maxDistance: distance

	  }
	});
	
	// find exec...
	
}	


exports.getJobsInCategory = function (req, res) {
	console.log('Getting Jobs in Category');
	var querystr = req.params.querystr;
	var query = {jobCategoryId: querystr};
	
	Jobs.find(query).exec(function (err, jobs) {
                if (err) {
                    return res.send("Error retrieving Jobs in Category: " + err);
                } else {
                    return res.send(jobs);
                }
            });

}

exports.login = function (req, res) {
	console.log('Login');
	//console.log(req.body[0]); // login
	//console.log(req.body[1]); // raw-password
	query = {login:req.body[0]}
	User.findOne(query).exec(function (err, user) {
                if (err) {
                    return res.send("Error retrieving users: " + err);
                } else {
					if (!user){
						return res.send([]);
					}else{
						if (getHashPasswordPassword(req.body[1],user.salt) == user._password){
							let token = jwt.sign({login: user.login,_password: user._password, role: user.role}, config.appconf.jwt_secret, {
										expiresIn: config.appconf.jwt_token_expiration_time // expires in 1 hour
								});
								user.token = token;
								delete user.salt;
							return res.send([user]);
						}else{
							return res.send([]);
						}
					}
                }
            });

}

exports.login_by_app = function (req, res) {
	console.log('Login by app/refresh token');
	//console.log(req.body[0]); // login
	//console.log(req.body[1]); // hashed-password
	query = {login:req.body[0],_password: req.body[1]}
	
	User.findOne(query,{salt: 0}).exec(function (err, user) {
                if (err) {
                    return res.send("Error retrieving users: " + err);
                } else {
					if (!user){
						return res.send([]);
					}else{
						let token = jwt.sign({login: user.login,_password: user._password, role: user.role}, config.appconf.jwt_secret, {
									expiresIn: config.appconf.jwt_token_expiration_time // expires in 1 hour
							});
							user.token = token;
						return res.send([user]);
					}
                }
            });

}

var gethash_at_sha512 = function(_password, salt){
    var hash = crypto.createHmac('sha512', salt);
    hash.update(_password);
    var value = hash.digest('hex');
	return value;
};

function getHashPasswordPassword(userpassword,salt) {
    var passwordData = gethash_at_sha512(userpassword, salt);
	return passwordData;
}



var async = require('async'),
    BOIModel = require('../models/boi_entities.js');
	mongoose = require('mongoose'),
    JobCategories = mongoose.model('JobCategories'),
	JobSubCategories = mongoose.model('JobSubCategories'),
	Jobs = mongoose.model('Jobs'),
	Favourite = mongoose.model('Favourite'),
	Notifications = mongoose.model('Notifications'),
	
	User = mongoose.model('User'),
    config = require('../config/config.js'),
    MongoClient = require('mongodb').MongoClient;
const s3_controller = require('./s3file_controller')
const fs = require('fs')
var jwt = require('jsonwebtoken');	
const crypto = require('crypto');	
	
exports.addCategory = function (req, res) {
	console.log('Adding Job Category');
	var jobCategoryName = req.body.jobCategoryName;
	var jobDescription = req.body.jobDescription;
	//console.log("body: " + JSON.stringify(req.body));
	//console.log("file: " + JSON.stringify(req.file));
	//console.log("jobCategoryName: " + jobCategoryName);

	if (!jobCategoryName){
		res.status = 400;
		return res.send("fail");
	}
	var jobCategory = new JobCategories();
	jobCategory.jobCategoryName = jobCategoryName;
	if (jobDescription){
		jobCategory.jobDescription = jobDescription;
	}	
	
	if (req.file && req.file.path){
		var jobCategoryPicture = req.file.path;
		var filename ='uploads/' + req.file.filename + '.png';
		var file = fs.createReadStream(jobCategoryPicture);
		s3_controller.s3upload(filename,file,(err, result) => {
			fs.unlinkSync(jobCategoryPicture);
			if(err) return res.send(err)
			else {
				jobCategory.imageUrl = filename;
				jobCategory.save(function (err, savedJobCategory) {
					if (err || !savedJobCategory) {
						console.log(err);
						return res.send("Error adding Job Category: " + err);
					}else{
						console.log("new category added, category name: " + jobCategoryName);
						return res.send("success");
					}
				});
			}
		});
	}else{
		jobCategory.save(function (err, savedJobCategory) {
			if (err || !savedJobCategory) {
				console.log(err);
				res.status = 400;
				return res.send("Error adding Job Category: " + err);
			}else{
				console.log("new category added, category name: " + jobCategoryName);
				return res.send("success");
			}
		});
	}	
}

exports.addJob = function (req, res) {
	console.log('Add Job');
	var jobName = req.body.jobName;
	var jobDescription = req.body.jobDescription;
	var categorySpinner = req.body.categorySpinner;
	var subCategorySpinner = req.body.subCategorySpinner;
	
	var longitude = parseFloat(req.body.longitude, 10);
	var latitude = parseFloat(req.body.latitude, 10);
	var jobLocationName = req.body.locationAddress;
	var ownerId = req.body.userId;
	

	//console.log("jobName: "+jobName);
	//console.log("categorySpinner: "+categorySpinner);
	//console.log("req.body: "+JSON.stringify(req.body));
	//console.log("req.params: "+JSON.stringify(req.params));
	//console.log("req.file: "+JSON.stringify(req.file));
	
	if (!ownerId) {
		res.status = 400;
		return res.send("ownerId is required.");
	}
	
	if (!jobName) {
		res.status = 400;
		return res.send("jobName is required.");
	}
	if (!categorySpinner) {
		res.status = 400;
		return res.send("categorySpinner is required.");		
	}	
	if (!jobLocationName) {
		res.status = 400;
		return res.send("jobLocationName is required.");
	}
	if (!longitude) {
		res.status = 400;
		return res.send("longitude is required.");
	}
	console.log("req.body: longitude: "+longitude);
	if (!latitude) {
		res.status = 400;
		return res.send("latitude is required.");
	}	

	var job = new Jobs();
	job.jobName = jobName;
	job.ownerId = ownerId;
	
	if (jobDescription){
		job.jobDescription = jobDescription;
	}
	job.categoryId = categorySpinner;
	
	
	job.jobLocationName = jobLocationName;
	job.geo = [latitude,longitude];
	
	if (subCategorySpinner){
		var test = subCategorySpinner.split(",");
		job.subCategoryId = [];
		for(var i=0;i<test.length;i++){
			console.log("array item: "+test[i]);
			job.subCategoryId.push(test[i]);
		}
	}
	

	if (req.file && req.file.path){
		var jobPicture = req.file.path;
		var filename ='uploads/' + req.file.filename + '.png';
		var file = fs.createReadStream(jobPicture);
		s3_controller.s3upload(filename,file,(err, result) => {
			fs.unlinkSync(jobPicture);
			if(err) return res.send(err)
			else {
				job.imageUrl = filename;
				job.save(function (err, savedJob) {
					if (err || !savedJob) {
						console.log(err);
						return res.send("Error adding Job: " + err);
					}else{
						console.log("New job added, JobName: " + jobName);
						return res.send("success");
					}
				});
			}
		});
	}else{
		job.save(function (err, savedJob) {
			if (err || !savedJob) {
				console.log(err);
				res.status = 400;
				return res.send("Error adding Job: " + err);
			}else{
				console.log("New job added, JobName: " + jobName);
				return res.send("success");
			}
		});
	}

}

exports.addFacebookUserIfNotExist = function (req, res) {
	console.log('Add Facebook User if not exist');
	//console.log(req.body[0]); // id
	//console.log(req.body[1]); // name
	//console.log(req.body[2]); // email
	
	User.findOne({facebook_id : req.body[0]},{salt: 0}).exec(function (err, user) {
                if (err) {
                    return res.send("Error retrieving user from facebook id: " + err);
                } else {
					
					if (!user){
						console.log("user doesnt exist need to add it");
						var user = new User();
						user.login = req.body[0];
						user.facebook_id = req.body[0];
						//{user.password, user.salt} = saltHashPassword(makeid());
						var resul = saltHashPassword(makeid());
						user._password = resul["passwordHash"];
						user.salt = resul["salt"];
						user.name = req.body[1];
						user.email = req.body[2];
						user.save(function (err, new_user) {
											if (err || !new_user) {
												console.log(err);
												return res.send("Error adding facebook user to db: " + err);
											}else{
												console.log("New user added by facebook login: " + new_user.name);
												var result = [];
												let token = jwt.sign({login: user.login,_password: user._password, role: user.role}, config.appconf.jwt_secret, {
													expiresIn: config.appconf.jwt_token_expiration_time // expires in 1 hour
												});
												user.token = token;
												delete user.salt;
												result.push(user);
												res.send(result);
											}
										});
					}else{
						var result = [];
						extra = {};
						extra["old"] = true;
						let token = jwt.sign({login: user.login,_password: user._password, role: user.role}, config.appconf.jwt_secret,{expiresIn: config.appconf.jwt_token_expiration_time});
						user.token = token;
						result.push(user);
						result.push(extra);
						res.send(result);
					}
                }
            });
}


exports.addGoogleUserIfNotExist = function (req, res) {
	console.log('Add Google User if not exist');
	//console.log(req.body[0]); // id
	//console.log(req.body[1]); // name
	//console.log(req.body[2]); // email
	
	User.findOne({google_plus_id : req.body[0]},{salt: 0}).exec(function (err, user) {
                if (err) {
                    return res.send("Error retrieving user from facebook id: " + err);
                } else {
					
					if (!user){
						console.log("user doesnt exist need to add it");
						var user = new User();
						user.login = req.body[0];
						user.google_plus_id = req.body[0];
						var resul = saltHashPassword(makeid());
						user._password = resul["passwordHash"];
						user.salt = resul["salt"];
						user.name = req.body[1];
						user.email = req.body[2];
						user.save(function (err, new_user) {
											if (err || !new_user) {
												console.log(err);
												return res.send("Error adding google user to db: " + err);
											}else{
												console.log("New user added by google login: " + new_user.name);
												var result = [];
												let token = jwt.sign({login: user.login,_password: user._password, role: user.role}, config.appconf.jwt_secret, {
													expiresIn: config.appconf.jwt_token_expiration_time // expires in 1 hour
												});
												user.token = token;
												delete user.salt;
												result.push(user);
												res.send(result);
											}
										});
					}else{
						var result = [];
						extra = {};
						extra["old"] = true;
						let token = jwt.sign({login: user.login,_password: user._password, role: user.role}, config.appconf.jwt_secret,{expiresIn: config.appconf.jwt_token_expiration_time});
						user.token = token;
						result.push(user);
						result.push(extra);
						res.send(result);
					}
                }
            });
}




exports.addFavourite = function(req, res){
    let favourite = new Favourite();
		favourite.userId = req.body[0];
		favourite.jobId = req.body[1];

		favourite.save(function(err, favourite){

        if(err){
			console.log("error " + err);
            return res.send({error: true});
        }
	
        res.send(["success"]);
    })
}

exports.addNotification = function(req, res){
	console.log("adding notification.");
    let notification = new Notifications();
		notification.jobId = req.body[0];
		notification.subject = req.body[1];
		notification.description = req.body[2];
		notification.type = req.body[3];

		notification.save(function(err, notification){

        if(err){
			console.log("error " + err);
            return res.send({error: true});
        }
	
        res.send(["success"]);
    })
}

exports.normalSignUp = function(req, res){
    let user = new User();
		user.login = req.body[0];
		var resul = saltHashPassword(req.body[1]);
		user._password = resul["passwordHash"];
		user.salt = resul["salt"];
		
		user.email = req.body[2];
		user.name = req.body[3];

		user.save(function(err, user){

        if(err){
			console.log("error " + err);
            return res.send({error: true});
        }
		delete user.salt;
		let token = jwt.sign({login: user.login,_password: user._password, role: user.role}, config.appconf.jwt_secret,{expiresIn: config.appconf.jwt_token_expiration_time});
		user.token = token;		
        res.send([user]);
    })
}


exports.addSubCategory = function(req, res){
	
    let jobSubCategory = new JobSubCategories();
	jobSubCategory.subCategoryName = req.body[0];
	jobSubCategory.subCategoryDescription = req.body[1];
	jobSubCategory.categoryId = req.body[2];

	jobSubCategory.save(function(err, user){

    if(err){
		console.log("error " + err);
        return res.send({error: true});
    }	
	console.log("Sub category was added");
    res.send(["success"]);
    })
}






exports.registerJob = function(req,res){
	console.log("reached protected by login location");
	res.send("reached protected location");
}




var genRandomString = function(length){
    return crypto.randomBytes(Math.ceil(length/2))
            .toString('hex') /** convert to hexadecimal format */
            .slice(0,length);   /** return required number of characters */
};

var sha512 = function(_password, salt){
    var hash = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
    hash.update(_password);
    var value = hash.digest('hex');
    return {
        salt:salt,
        passwordHash:value
    };
};

function saltHashPassword(userpassword) {
    var salt = genRandomString(16); /** Gives us salt of length 16 */
    var passwordData = sha512(userpassword, salt);
	return passwordData;
}



function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 15; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}



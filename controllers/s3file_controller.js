const AWS = require('aws-sdk')
const async = require('async')
const path = require('path')
const fs = require('fs')
const config = require('../config/config')

/** Load Config File */
AWS.config.loadFromPath('controllers/config.json')

/** After config file load, create object for s3*/
const s3 = new AWS.S3({region: config.appconf.s3_options.region})

exports.s3upload = (filename, content, callback) => {
  const params = { 
        Bucket: config.appconf.s3_options.bucket, 
        Key: filename, 
        ACL: 'public-read',
        Body:content
    };
	s3.putObject(params, function (err, data) {
		if (err) {
	    	console.log("Error uploading file: ", err);
	    	callback(err, null)
	    } else {
	    	console.log("Successfully uploaded file on S3", data);
	    	callback(null, data)
	    }
	})  
}


exports.displayForm = (req, res) => {
    res.writeHead(200, {
        "Content-Type": "text/html"
    });
    res.write(
        '<form action="/upload" method="post" enctype="multipart/form-data">' +
        '<input type="file" name="file">' +
        '<input type="submit" value="Upload">' +
        '</form>'
    );
    res.end();
};
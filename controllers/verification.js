var jwt = require('jsonwebtoken');
var config = require('../config/config.js');

exports.bearer = function(req,res,next) {
  var token = req.body.token || req.query.token || req.headers['x-access-token'];

    if (token) {
    // verifies secret and checks exp
        jwt.verify(token, config.appconf.jwt_secret, function(err, decoded) {
            if (err) { //failed verification.
				res.setHeader('error', 'expired');
                return res.status(403).send("expired");
            }
            req.decoded = decoded;
			
            next(); //no error, proceed
        });
    } else {
        // forbidden without token
        return res.status(403).send("no token");
    }
}

exports.admin = function(req,res,next) {
  var token = req.body.token || req.query.token || req.headers['x-access-token'];

    if (token) {
    // verifies secret and checks exp
        jwt.verify(token, config.appconf.jwt_secret, function(err, decoded) {
            if (err) { //failed verification.
				res.setHeader('error', 'expired');
                return res.status(403).send("expired");
            }
            req.decoded = decoded;
			if (!decoded.role || decoded['role'] != "admin"){
				console.log("admin error decoded");
				return res.status(403).send("route is classified for this user.");
			}
            next(); //no error, proceed
        });
    } else {
        // forbidden without token
        return res.status(403).send("no token");
    }
}
'use strict';
var express = require('express'), appRequestsCtrl=require('../controllers/app_requests_controller.js');


exports.appRoutes = function() {
	
	app.get('/getjobcategories', appRequestsCtrl.getJobCategories);
	app.get('/getjobsincategory/:querystr', appRequestsCtrl.getJobsInCategory);
	app.post('/login', appRequestsCtrl.login);
	app.post('/refreshtoken', appRequestsCtrl.login_by_app);
	app.post('/getsubcategories', appRequestsCtrl.getSubCategories);
	app.post('/getfavorites', appRequestsCtrl.getFavorites);
	app.get('/getjobnotifications/:querystr', appRequestsCtrl.getNotifications);
	app.get('/getjobdetails/:querystr', appRequestsCtrl.getJobDetails);
	app.get('/.well-known/assetlinks.json', appRequestsCtrl.getAppLink);
	app.get('/test', appRequestsCtrl.test);
}
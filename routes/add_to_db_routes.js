'use strict';
var express = require('express'), addToDbCtrl=require('../controllers/add_requests_controller.js'), verificationCtrl=require('../controllers/verification.js');
var multer  = require('multer')
var upload = multer({dest:'./upload/'})
let verifyToken = require('../controllers/verification');

exports.addToDbRoutes = function() {
	
	
	
	app.post('/addfacebookuser', addToDbCtrl.addFacebookUserIfNotExist);
	app.post('/addgoogleuser', addToDbCtrl.addGoogleUserIfNotExist);
	app.post('/signup', addToDbCtrl.normalSignUp);
	
	app.post('/addjobcategory',verificationCtrl.admin, upload.single("picture"), addToDbCtrl.addCategory);
	app.post('/addsubcategory',verificationCtrl.admin, addToDbCtrl.addSubCategory);

	
	app.post('/addnewjob', verificationCtrl.bearer, upload.single("picture"), addToDbCtrl.addJob);
	
	app.post('/addfavouritejob', verificationCtrl.bearer, addToDbCtrl.addFavourite);
	app.post('/addnotification', verificationCtrl.bearer, addToDbCtrl.addNotification);
	

}